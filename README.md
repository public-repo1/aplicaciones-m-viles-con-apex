# Aplicaciones Móviles con APEX

Blog [aflorestorres](https://aflorestorres.com).

[Allmylinks](https://allmylinks.com/aflorestorres).

App Demo

user and pss: demo

[https://bit.ly/3axiwtw](https://bit.ly/3axiwtw).

Presentación

1) Reflow Report

    [Reflow Report](https://apex.oracle.com/pls/apex/apex_pm/r/ut/reflow-report)


2) Cambiar el reporte a una navegación horizontal  (tipo netflix)


3) Responsive Utilities

    [Grid layout](https://apex.oracle.com/pls/apex/apex_pm/r/ut/grid-layout)

    [Responsive Utilities](https://apex.oracle.com/pls/apex/apex_pm/r/ut/responsive-utilities)

4) No refrescar la página
    Usar show y hide   

5) Pensar en AJAX

    [apex.server](https://docs.oracle.com/en/database/oracle/application-express/20.1/aexjs/apex.server.html#.process)


---------

[PWA](http://vmorneau.me/tag/pwa/)
    
[FABE](https://forallabeautifulearth.org/home/fabe-app/)

[Universal Theme](https://apex.oracle.com/pls/apex/apex_pm/r/ut/home)